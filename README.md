# Jenkins と GitLab を連携する練習のためのリポジトリ

## 参考ページ

以下はGitLabではなく、GitLabとの連携の方法を書いた

[DockerでJenkinsコンテナを作りGithubと連携する | mrsekutの備忘録](http://marumaru.tonkotsu.jp/docker%E3%81%A7jenkins%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E3%82%92%E4%BD%9C%E3%82%8Agithub%E3%81%A8%E9%80%A3%E6%90%BA%E3%81%99%E3%82%8B)


## 概要

詳細は上の参考ページにまとめた


## Jenkins側の設定

1. 「Jenkinsの管理」からgitlab用のプラグインを導入する
1. 「GitLab plugin」をインストール
2. jobを作成して、settingへ
2. 以下の項目を埋める
    - ソースコード管理
        - git(リポジトリURLにgitlabのプロジェクトのurl)
        - リポジトリ・ブラウザ(gitlab,url:gitlbaのプロジェクトのurl, Version:gitlabのバージョン)
    - ビルド・トリガ
        - Buil When a change is pushed to GitLabにチェックを付ける。
        - ↑このチェックを付けたときに表示されるURLをメモっておく
        - SCMをポーリング(スケジュール: * * * * * )
    - ビルド
        - シェルの実行(シェルスクリプト: python test.py)

## GitLab側の設定

1. プロジェクトページから左にあるメニューのSetting→Integrations
2. urlの欄に先程メモったURLを貼り付ける
3. Save changes


## 試してみる

これで準備が整ったので、適当にコードを変更して、pushをすると2,3分後にビルドが開始されて、テストコードが実行される。
テストに失敗すると、ビルドも失敗になる.
